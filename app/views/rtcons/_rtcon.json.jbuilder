json.extract! rtcon, :id, :created_at, :updated_at
json.url rtcon_url(rtcon, format: :json)
