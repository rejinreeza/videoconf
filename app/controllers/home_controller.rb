class HomeController < ApplicationController
  before_action :authenticate_user!
  def index
    @other_users = User.where.not(id: current_user.id)
    @channel_status  = current_user.rtc_id ? true : false

  end

  def create_channel
    current_user.update_attributes(:rtc_id => SecureRandom.uuid)
    redirect_to home_index_path
  end


  def host 
    
  end

  def client
    @user = User.find(params[:uid])
  end
end
