class RtconsController < ApplicationController
  before_action :set_rtcon, only: [:show, :edit, :update, :destroy]

  # GET /rtcons
  # GET /rtcons.json
  def index
    @rtcons = Rtcon.all
  end

  # GET /rtcons/1
  # GET /rtcons/1.json
  def show
  end

  # GET /rtcons/new
  def new
    @rtcon = Rtcon.new
  end

  # GET /rtcons/1/edit
  def edit
  end

  # POST /rtcons
  # POST /rtcons.json
  def create
    @rtcon = Rtcon.new(rtcon_params)

    respond_to do |format|
      if @rtcon.save
        format.html { redirect_to @rtcon, notice: 'Rtcon was successfully created.' }
        format.json { render :show, status: :created, location: @rtcon }
      else
        format.html { render :new }
        format.json { render json: @rtcon.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rtcons/1
  # PATCH/PUT /rtcons/1.json
  def update
    respond_to do |format|
      if @rtcon.update(rtcon_params)
        format.html { redirect_to @rtcon, notice: 'Rtcon was successfully updated.' }
        format.json { render :show, status: :ok, location: @rtcon }
      else
        format.html { render :edit }
        format.json { render json: @rtcon.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rtcons/1
  # DELETE /rtcons/1.json
  def destroy
    @rtcon.destroy
    respond_to do |format|
      format.html { redirect_to rtcons_url, notice: 'Rtcon was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rtcon
      @rtcon = Rtcon.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def rtcon_params
      params.fetch(:rtcon, {})
    end
end
