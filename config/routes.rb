Rails.application.routes.draw do
  devise_for :users
  get 'home/index'
  get 'home/create_channel'
  get 'home/host'
  get 'home/client'
  resources :rtcons
   root to: "home#index"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
