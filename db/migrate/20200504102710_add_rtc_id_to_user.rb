class AddRtcIdToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :rtc_id, :string
  end
end
