require "application_system_test_case"

class RtconsTest < ApplicationSystemTestCase
  setup do
    @rtcon = rtcons(:one)
  end

  test "visiting the index" do
    visit rtcons_url
    assert_selector "h1", text: "Rtcons"
  end

  test "creating a Rtcon" do
    visit rtcons_url
    click_on "New Rtcon"

    click_on "Create Rtcon"

    assert_text "Rtcon was successfully created"
    click_on "Back"
  end

  test "updating a Rtcon" do
    visit rtcons_url
    click_on "Edit", match: :first

    click_on "Update Rtcon"

    assert_text "Rtcon was successfully updated"
    click_on "Back"
  end

  test "destroying a Rtcon" do
    visit rtcons_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Rtcon was successfully destroyed"
  end
end
