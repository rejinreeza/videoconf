require 'test_helper'

class RtconsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rtcon = rtcons(:one)
  end

  test "should get index" do
    get rtcons_url
    assert_response :success
  end

  test "should get new" do
    get new_rtcon_url
    assert_response :success
  end

  test "should create rtcon" do
    assert_difference('Rtcon.count') do
      post rtcons_url, params: { rtcon: {  } }
    end

    assert_redirected_to rtcon_url(Rtcon.last)
  end

  test "should show rtcon" do
    get rtcon_url(@rtcon)
    assert_response :success
  end

  test "should get edit" do
    get edit_rtcon_url(@rtcon)
    assert_response :success
  end

  test "should update rtcon" do
    patch rtcon_url(@rtcon), params: { rtcon: {  } }
    assert_redirected_to rtcon_url(@rtcon)
  end

  test "should destroy rtcon" do
    assert_difference('Rtcon.count', -1) do
      delete rtcon_url(@rtcon)
    end

    assert_redirected_to rtcons_url
  end
end
